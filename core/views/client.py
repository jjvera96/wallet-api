""" Contains Client endpoint definition """
from .custom_model_view_set import CustomModelViewSet
from ..models.client import Client
from ..serializers.client import ClientSerializer


class ClientViewSet(CustomModelViewSet):
    serializer_class = ClientSerializer

    def get_queryset(self):
        q = {'is_active': True}
        if not self.request.user.is_staff:
            q['user'] = self.request.user
        instances = Client.objects.filter(**q)
        return instances
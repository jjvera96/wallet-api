""" Contains Auth endpoint definition """
import datetime as dt
import jwt
from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.crypto import get_random_string
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from ..models.auth import Auth
from ..serializers.auth import AuthSerializer
from ..serializers.user import UserSerializer


class AuthApi(APIView):
    """ Defines the HTTP verbs to auth model management. """

    def post(self, request):
        """ Creates a new session.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (int)
                Response status code.

        """
        serializer = AuthSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'code': 'invalid_body',
                'detailed': 'Body with invalid structure.',
                'data': serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)

        user = get_user_model().objects.filter(
            Q(email=request.data['user']) | 
            Q(username=request.data['user']), is_active=True).first()

        if not user:
            return Response({
                'code': 'user_not_found',
                'detailed': 'User not found or no active.'
            }, status=status.HTTP_404_NOT_FOUND)

        if not check_password(request.data["password"], user.password):
            return Response({
                'code': 'incorrect_password',
                'detailed': 'Invalid credentials.'
            }, status=status.HTTP_401_UNAUTHORIZED)

        refresh = get_random_string(30)
        token = jwt.encode({
            'expiration_date': str(
                (
                    dt.datetime.now() +
                    dt.timedelta(hours=settings.TOKEN_EXP_HOURS)
                )
            ),
            'username': user.username,
            'refresh': refresh
        }, settings.SECRET_KEY, algorithm='HS256')
        user.auths.update(is_disabled=True)
        get_user_model().objects.filter(
            email=user.email, is_active=True
        ).update(last_login=dt.datetime.now())
        Auth.objects.create(token=token, user=user)
        return Response({
            'access_token': token,
            'refresh_token': refresh,
            'user': UserSerializer(user).data            
        }, status=status.HTTP_201_CREATED)

    def patch(self, request):  
        """ Disables a session.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (int)
                Response status code.

        """
        header = request.headers.get('Authorization', None)

        if (not header or len(header.split(' ')) != 2 or
                header.split(' ')[0].lower() != 'bearer'):
            return Response({
                'code': 'no_token',
                'detailed': 'Petition without token.'
            }, status=status.HTTP_401_UNAUTHORIZED)

        session = Auth.objects.filter(token=header.split(' ')[1])
        if not session:
            return Response({
                'code': 'token_not_found',
                'detailed': 'Token does not exist.'
            }, status=status.HTTP_404_NOT_FOUND)

        session.update(is_disabled=True)
        return Response(status=status.HTTP_200_OK)


class RefreshTokenApi(APIView):
    """ Defines the HTTP verbs to refresh token. """

    def patch(self, request, *args, **kwargs):
        """ Refreshes a token.

        Parameters
        ----------

        request (dict)
            Contains http transaction information.

        Returns
        -------
            Response (int)
                Response status code.

        """
        header = request.headers.get('Authorization', None)

        if (not header or len(header.split(' ')) != 2 or
                header.split(' ')[0].lower() != 'bearer'):
            return Response({
                'code': 'no_token',
                'detailed': 'Petition without token.'
            }, status=status.HTTP_401_UNAUTHORIZED)
        try:
            token = jwt.decode(header.split(' ')[1], settings.SECRET_KEY, algorithms=['HS256'])
        except jwt.InvalidTokenError:
            return Response({
                'code': 'invalid_token',
                'detailed': 'Invalid token.'
            }, status=status.HTTP_401_UNAUTHORIZED)

        if token['refresh'] != kwargs['token']:
            return Response({
                'code': 'invalid_refresh_token',
                'detailed': 'Invalid refresh token.'
            }, status=status.HTTP_409_CONFLICT)
        
        if((dt.datetime.now() + dt.timedelta(hours=1)) > 
            dt.datetime.strptime(token['expiration_date'], '%Y-%m-%d %H:%M:%S.%f')):
            return Response({
                'code': 'expired_refresh_token',
                'detailed': 'Expired refresh token.'
            }, status=status.HTTP_403_FORBIDDEN)

        user = get_user_model().objects.filter(
            email=token['username'], is_active=True).first()
        if not user:
            return Response({
                'code': 'user_not_found',
                'detailed': 'User not found or no active.'
            }, status=status.HTTP_404_NOT_FOUND)

        refresh = get_random_string(30)
        token = jwt.encode({
            'expiration_date': str(
                (
                    dt.datetime.now() +
                    dt.timedelta(hours=settings.TOKEN_EXP_HOURS)
                )
            ),
            'username': user.username,
            'refresh': refresh
        }, settings.SECRET_KEY, algorithm='HS256')

        session = Auth.objects.filter(token=header.split(" ")[1]).first()
        if not session:
            return Response({
                'code': 'token_not_found',
                'detailed': 'Token does not exist.'
            }, status=status.HTTP_404_NOT_FOUND)
        
        if session.is_disabled:
            return Response({
                'code': 'expired_refresh_token',
                'detailed': 'Expired refresh token.'
            }, status=status.HTTP_409_CONFLICT)

        Auth.objects.filter(token=header.split(' ')[1]).update(token=token)
        return Response({
            'access_token': token,
            'refresh_token': refresh,
            'user': UserSerializer(user).data
        }, status=status.HTTP_201_CREATED)

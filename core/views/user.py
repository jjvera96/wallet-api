""" Contains User endpoint definition """
from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework import status
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from ..permissions.authentication_user import AuthenticationUser
from ..permissions.is_admin_or_owner import IsAdminOrOwner
from ..serializers.user import CreateUserSerializer
from ..serializers.user import UpdateUserSerializer
from ..serializers.user import UserSerializer


class UserCreate(generics.CreateAPIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = CreateUserSerializer

class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = [AuthenticationUser]
    queryset = get_user_model().objects.all()

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [IsAdminUser]
        else: 
            permission_classes = [IsAdminOrOwner]
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return UpdateUserSerializer
        else:
            return UserSerializer

    def create(self, request, *args, **kwargs):
        return Response({'code': 'not_found','detail': 'Not found.'}, status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if not request.user.is_staff:
            if instance.user != request.user:
                return Response({
                        "code": "not_authorized",
                        "detailed": "Not authorized."
                    }, status=status.HTTP_401_UNAUTHORIZED)
        instance.is_active = False
        instance.save()
        return Response(status=status.HTTP_200_OK)
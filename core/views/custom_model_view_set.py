""" Contains Custom Model View Set definition """
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from ..permissions.authentication_user import AuthenticationUser
from ..permissions.is_admin_or_owner import IsAdminOrOwner
from ..utils.paginator import CustomPagination


class CustomModelViewSet(viewsets.ModelViewSet):
    authentication_classes = [AuthenticationUser]
    permission_classes = [IsAdminOrOwner]
    pagination_class = CustomPagination


    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if not request.user.is_staff:
            if instance.user != request.user:
                return Response({
                        "code": "not_authorized",
                        "detailed": "Not authorized."
                    }, status=status.HTTP_401_UNAUTHORIZED)
            if not instance.is_active:
                return Response({
                        "code": "not_found",
                        "detailed": "Not found."
                    }, status=status.HTTP_401_UNAUTHORIZED)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if not request.user.is_staff:
            if instance.user != request.user:
                return Response({
                        "code": "not_authorized",
                        "detailed": "Not authorized."
                    }, status=status.HTTP_401_UNAUTHORIZED)
        instance.is_active = False
        instance.save()
        return Response(status=status.HTTP_200_OK)


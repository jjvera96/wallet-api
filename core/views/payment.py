""" Contains Payment endpoint definition """
from .custom_model_view_set import CustomModelViewSet
from ..models.payment import Payment
from ..serializers.payment import PaymentSerializer


class PaymentViewSet(CustomModelViewSet):
    serializer_class = PaymentSerializer

    def get_queryset(self):
        q = {'is_active': True}
        if not self.request.user.is_staff:
            q['user'] = self.request.user
        instances = Payment.objects.filter(**q)
        return instances
""" Contains Debt endpoint definition """
from rest_framework import status
from rest_framework.response import Response
from .custom_model_view_set import CustomModelViewSet
from ..models.debt import Debt
from ..serializers.debt import DebtSerializer


class DebtViewSet(CustomModelViewSet):
    serializer_class = DebtSerializer

    def get_queryset(self):
        q = {'is_active': True}
        if not self.request.user.is_staff:
            q['user'] = self.request.user
        instances = Debt.objects.filter(**q)
        return instances
    
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if not request.user.is_staff:
            if instance.user != request.user:
                return Response({
                        "code": "not_authorized",
                        "detailed": "Not authorized."
                    }, status=status.HTTP_401_UNAUTHORIZED)
        instance.is_active = False
        instance.payments.all().update(is_active=False)
        instance.save()
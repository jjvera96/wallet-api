""" Contains Lending endpoint definition """
from rest_framework import status
from rest_framework.response import Response
from .custom_model_view_set import CustomModelViewSet
from ..models.lending import Lending
from ..serializers.lending import LendingSerializer


class LendingViewSet(CustomModelViewSet):
    serializer_class = LendingSerializer

    def get_queryset(self):
        q = {'is_active': True}
        if not self.request.user.is_staff:
            q['user'] = self.request.user
        instances = Lending.objects.filter(**q)
        return instances

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if not request.user.is_staff:
            if instance.user != request.user:
                return Response({
                        "code": "not_authorized",
                        "detailed": "Not authorized."
                    }, status=status.HTTP_401_UNAUTHORIZED)
        instance.is_active = False
        instance.payments.all().update(is_active=False)
        instance.save()

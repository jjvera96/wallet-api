""" Contains Egress endpoint definition """
from .custom_model_view_set import CustomModelViewSet
from ..models.egress import Egress
from ..serializers.egress import EgressSerializer


class EgressViewSet(CustomModelViewSet):
    serializer_class = EgressSerializer

    def get_queryset(self):
        q = {'is_active': True}
        if not self.request.user.is_staff:
            q['user'] = self.request.user
        instances = Egress.objects.filter(**q)
        return instances

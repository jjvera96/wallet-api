""" Contains Income endpoint definition """
from .custom_model_view_set import CustomModelViewSet
from ..models.income import Income
from ..serializers.income import IncomeSerializer


class IncomeViewSet(CustomModelViewSet):
    serializer_class = IncomeSerializer

    def get_queryset(self):
        q = {'is_active': True}
        if not self.request.user.is_staff:
            q['user'] = self.request.user
        instances = Income
        Income.objects.filter(**q)
        return instances
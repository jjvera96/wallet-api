""" Contains Advice endpoint definition """
from random import randint
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from ..models.advice import Advice
from ..serializers.advice import AdviceSerializer


class AdviceApi(APIView):
    """ Defines the HTTP verbs to advice model management. """

    def get(self, request, *args, **kwargs):
        last = Advice.objects.count()
        if not last:
            return Response({
                "code": "not_advices",
                "detailed": "There are not advices in database."
            }, status=status.HTTP_404_NOT_FOUND)
        object = Advice.objects.get(id=randint(1, last))
        return Response(AdviceSerializer(object).data)

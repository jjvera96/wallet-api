from django.contrib import admin
from .models.advice import Advice
from .models.auth import Auth
from .models.client import Client
from .models.debt import Debt
from .models.egress import Egress
from .models.income import Income
from .models.lending import Lending
from .models.payment import Payment


# Register your models here.
class LendingAdmin(admin.ModelAdmin):
    filter_horizontal = ('payments', )

class DebtAdmin(admin.ModelAdmin):
    filter_horizontal = ('payments', )

admin.site.register(Advice)
admin.site.register(Auth)
admin.site.register(Client)
admin.site.register(Debt, DebtAdmin)
admin.site.register(Egress)
admin.site.register(Income)
admin.site.register(Lending, LendingAdmin)
admin.site.register(Payment)

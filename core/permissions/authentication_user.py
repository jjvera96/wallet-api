""" Contains authentication permission for managing views """
import jwt
import datetime as dt
from django.conf import settings
from rest_framework.authentication import BaseAuthentication
from ..models.auth import Auth
from ..exceptions import NotAuthenticated


class AuthenticationUser(BaseAuthentication):
    """ 
    Check if the request has a valid user jwt 
    """

    def authenticate(self, request):
        header = request.headers.get("Authorization", None)

        if (not header or len(header.split(" ")) != 2 or
                header.split(" ")[0].lower() != "bearer"):
            raise NotAuthenticated()
        try:
            token = jwt.decode(header.split(" ")[1], settings.SECRET_KEY, algorithms=['HS256'])
        except jwt.InvalidTokenError:
            raise NotAuthenticated({
                "code": "invalid_token",
                "detailed": "Invalid token."
            })

        expiration_date = dt.datetime.strptime(
            token['expiration_date'], '%Y-%m-%d %H:%M:%S.%f')

        db_token = Auth.objects.filter(token=header.split(" ")[1]).first()
        if not db_token:
            raise NotAuthenticated({
                "code": "invalid_token",
                "detailed": "Invalid token."
            })

        if (expiration_date < dt.datetime.now() or db_token.is_disabled):
            raise NotAuthenticated({
                "code": "expired_token",
                "detailed": "Expired token."
            })
        return (db_token.user, None)

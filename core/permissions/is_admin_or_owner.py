""" Contains permission of admin or owner for managing views """
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import BasePermission
import re


class IsAdminOrOwner(BasePermission):
    """
    Check if who makes the petition is the admin or owner of the object
    """

    def has_permission(self, request, view):
        petition_user = re.search(r'api/user/(?P<user>[0-9]+)/', request.path).group('user')
        if request.user.id != int(petition_user) and not request.user.is_staff:
            raise PermissionDenied({
                "code": "no_authorized",
                "detailed": "No authorized."
            })

        if request.method == 'POST':
            request.data['user'] = petition_user

        return True

from django.urls import path
from rest_framework.routers import DefaultRouter
from .views.advice import AdviceApi
from .views.auth import AuthApi
from .views.auth import RefreshTokenApi
from .views.client import ClientViewSet
from .views.debt import DebtViewSet
from .views.egress import EgressViewSet
from .views.income import IncomeViewSet
from .views.lending import LendingViewSet
from .views.payment import PaymentViewSet
from .views.user import UserCreate
from .views.user import UserViewSet


router = DefaultRouter()
router
router.register(r'user/(?P<user>[0-9]+)/client', ClientViewSet, 'client')
router.register(r'user/(?P<user>[0-9]+)/debt', DebtViewSet, 'debt')
router.register(r'user/(?P<user>[0-9]+)/egress', EgressViewSet, 'egress')
router.register(r'user/(?P<user>[0-9]+)/deposit', IncomeViewSet, 'deposit')
router.register(r'user/(?P<user>[0-9]+)/loan', LendingViewSet, 'loan')
router.register(r'user/(?P<user>[0-9]+)/payment', PaymentViewSet, 'payment')
router.register(r'user', UserViewSet, 'user')


urlpatterns = [
    path('advice/', AdviceApi.as_view(), name='advice'),
    path('user/login/', AuthApi.as_view(), name='login'),
    path('user/refresh_token/<str:token>/', RefreshTokenApi.as_view(), name='refresh-token'),
    path('user/create/', UserCreate.as_view(), name='user-create'),
    
] + router.urls

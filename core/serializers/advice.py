""" Contains the Auth serializer """
from rest_framework import serializers
from ..models.advice import Advice


class AdviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Advice
        fields = '__all__'

""" Contains the Egress serializer """
from rest_framework import serializers
from ..models.egress import Egress


class EgressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Egress
        fields = '__all__'

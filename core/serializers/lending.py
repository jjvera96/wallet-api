""" Contains the Lending serializer """
from rest_framework import serializers
from ..models.lending import Lending


class LendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lending
        exclude = ['total']

    def create(self, validated_data):
        lending = Lending(
            user=validated_data['user'],
            client=validated_data['client'],
            title=validated_data['title'],
            description=validated_data['description'],
            amount=validated_data['amount'],
            percent=validated_data['percent'],
            total=(validated_data['amount'] + (validated_data['amount'])*validated_data['percent']/100)
        )
        lending.save()
        return lending

""" Contains the Debt serializer """
from rest_framework import serializers
from ..models.debt import Debt


class DebtSerializer(serializers.ModelSerializer):
    class Meta:
        model = Debt
        exclude = ['total']

    def create(self, validated_data):
        debt = Debt(
            user=validated_data['user'],
            client=validated_data['client'],
            title=validated_data['title'],
            description=validated_data['description'],
            amount=validated_data['amount'],
            percent=validated_data['percent'],
            total=(validated_data['amount'] + (validated_data['amount'])*validated_data['percent']/100)
        )
        debt.save()
        return debt
""" Contains the Income serializer """
from rest_framework import serializers
from ..models.income import Income


class IncomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Income
        exclude = '__all__'
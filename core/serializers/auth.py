""" Contains the Auth serializer """
from rest_framework import serializers


class AuthSerializer(serializers.Serializer):
    user = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

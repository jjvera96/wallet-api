""" Contains the Income model """
from django.db import models
from django.contrib.auth import get_user_model
from django_extensions.db.models import TimeStampedModel


class Income(TimeStampedModel):
    """ Income definition """
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    amount = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        """ Returns a string representation """
        return'{} - {}'.format(self.title, self.user.get_full_name())

    class Meta:
        """ Sets human readable name """
        verbose_name = "Ingreso"
        verbose_name_plural = "Ingresos"
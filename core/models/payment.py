""" Contains the Payment model """
from django.contrib.auth import get_user_model
from django.db import models
from django_extensions.db.models import TimeStampedModel


class Payment(TimeStampedModel):
    """ Payment definition """
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    amount = models.PositiveIntegerField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        """ Returns a string representation """
        return 'Pago {}'.format(self.pk)

    class Meta:
        """ Sets human readable name """
        verbose_name = "Pago"
        verbose_name_plural = "Pagos"
""" Contains the Debt model """
from django.db import models
from django.contrib.auth import get_user_model
from django_extensions.db.models import TimeStampedModel

from .client import Client
from .payment import Payment


class Debt(TimeStampedModel):
    """ Debt definition """
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    amount = models.PositiveIntegerField()
    percent = models.PositiveIntegerField(default=0)
    total = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now_add=True)
    payments = models.ManyToManyField(Payment, related_name='debt_payments', blank=True)
    state = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        """ Returns a string representation """
        return'{} {} - {}'.format(self.title, self.client.name, self.user.get_full_name())

    class Meta:
        """ Sets human readable name """
        verbose_name = "Deuda"
        verbose_name_plural = "Deudas"

""" Contains the Advice model """
from django.db import models
from django_extensions.db.models import TimeStampedModel


class Advice(TimeStampedModel):
    """ Advice definition """
    quote = models.CharField(max_length=255)

    def __str__(self):
        """ Returns a string representation """
        return'Advice {}'.format(self.id)

    class Meta:
        """ Sets human readable name """
        verbose_name = "Consejo"
        verbose_name_plural = "Consejos"
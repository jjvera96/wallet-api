""" Contains the Client model """
from django.db import models
from django.contrib.auth import get_user_model
from django_extensions.db.models import TimeStampedModel


class Client(TimeStampedModel):
    """ Client definition """
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    cellphone = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        """ Returns a string representation """
        return'{} - {}'.format(self.name, self.user.get_full_name())

    class Meta:
        """ Sets human readable name """
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"
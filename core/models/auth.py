""" Contains the Auth model """
from django_extensions.db.models import TimeStampedModel
from django.contrib.auth import get_user_model
from django.db import models


class Auth(TimeStampedModel):
    """ Auth definition for sessions. """  
    is_disabled = models.BooleanField(default=False)
    token = models.TextField("Token", max_length=700)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='auths')

    class Meta:
        """ Sets human readable name """
        verbose_name = "Sesión"
        verbose_name_plural = "Sesiones"
        
    def __str__(self):
        """ Returns a string representation"""
        return "{} - {}".format(self.user, self.created)
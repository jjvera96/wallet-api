from django_tqdm import BaseCommand
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from time import sleep
from ...models.advice import Advice
from ...models.client import Client
from ...models.debt import Debt
from ...models.egress import Egress
from ...models.income import Income
from ...models.lending import Lending
from ...models.payment import Payment


class Command(BaseCommand):
    help = 'Fill the database with the data.'

    def handle(self, *args, **options):
        tqdm = self.tqdm(total=8)
        tqdm.info('Starting to fill the database')
        tqdm.info('Create users')
        User = get_user_model()
        if User.objects.count() == 0:
            User.objects.create(
                username='admin',
                first_name='Admin',
                last_name='Admin',
                email='admin@admin.com',
                password=make_password('admin12345'),
                is_staff=True,
                is_superuser=True
            )
            fuser = User.objects.create(
                username='fuser',
                first_name='fUser',
                last_name='FUser',
                email='fuser@user.com',
                password=make_password('fuser12345'),
                is_staff=False,
                is_superuser=False
            )
            suser = User.objects.create(
                username='suser',
                first_name='SUser',
                last_name='SUser',
                email='suser@user.com',
                password=make_password('suser12345'),
                is_staff=False,
                is_superuser=False
            )
        else:
            fuser = User.objects.get(username='fuser')
            suser = User.objects.get(username='suser')
        tqdm.info('Users created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Create advices')
        if Advice.objects.count() == 0:
            Advice.objects.create(quote='Save money.')
            Advice.objects.create(quote='Save time.')
        tqdm.info('Advices created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Create clients')
        if Client.objects.count() == 0:
            fcfu = Client.objects.create(user=fuser, name='fclient fuser', cellphone='xxx xxx xxxx', address='xxx xxx xxx')
            scfu = Client.objects.create(user=fuser, name='sclient fuser', cellphone='xxx xxx xxxx', address='xxx xxx xxx')
            fcsu = Client.objects.create(user=suser, name='fclient suser', cellphone='xxx xxx xxxx', address='xxx xxx xxx')
        else:
            fcfu = Client.objects.get(name='fclient fuser')
            scfu = Client.objects.get(name='sclient fuser')
            fcsu = Client.objects.get(name='fclient suser')

        tqdm.info('Clients created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Create debts')
        if Debt.objects.count() == 0:
            fdfcfu = Debt.objects.create(user=fuser, client=fcfu, title='first debt', description='first debt', amount=100, percent=0, total=100, state=True)
            sdscfu = Debt.objects.create(user=fuser, client=scfu, title='second debt', description='second debt', amount=100, percent=0, total=100, state=False)
            fdfcsu = Debt.objects.create(user=suser, client=fcsu, title='first debt', description='second debt', amount=100, percent=10, total=110, state=False)
        else:
            fdfcfu = Debt.objects.get(user=fuser, client=fcfu, title='first debt')
            sdscfu = Debt.objects.get(user=fuser, client=scfu, title='second debt')
            fdfcsu = Debt.objects.get(user=suser, client=fcsu, title='first debt')
        tqdm.info('Debts created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Create egress')
        if Egress.objects.count() == 0:
            fefu = Egress.objects.create(user=fuser, title='first egress', description='first egress', amount=100)
        else:
            fefu = Egress.objects.get(user=fuser)
        tqdm.info('Egress created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Create incomes')
        if Income.objects.count() == 0:
            fefu = Income.objects.create(user=fuser, title='first income', description='first income', amount=100)
        else:
            fefu = Income.objects.get(user=fuser)
        tqdm.info('Incomes created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Create loans')
        if Lending.objects.count() == 0:
            flfu = Lending.objects.create(user=fuser, client=fcfu, title='first lending', description='first lending', amount=100, percent=0, state=False, total=100)
        else:
            flfu = Lending.objects.get(user=fuser)
        tqdm.info('Loans created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Create payments')
        if Payment.objects.count() == 0:
            fpfdfcfu = Payment.objects.create(user=fuser, amount=100)
            fdfcfu.payments.add(fpfdfcfu)
            fdfcfu.save()
            fpsdscfu = Payment.objects.create(user=fuser, amount=50)
            sdscfu.payments.add(fpsdscfu)
            sdscfu.save()
        tqdm.info('Payments created')
        sleep(1)
        tqdm.update(1)

        tqdm.info('Finished process')